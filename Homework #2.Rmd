---
title: 'HMW #2'
author: "Hank Greenburg"
date: "April 13, 2020"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Is there any evidence that mean or median salary for males exceeds mean or median salary for females with the same years of education and AFQT scores? If so, by how many dollars or by what percent is the male mean or median larger?

```{r,echo=FALSE}
library(Sleuth3)
library(ggplot2)
library(GGally)
library(tidyverse)
data = ex0923
head(data)
```


Here we can see the data set as well as the summary and confidence intervals. Which suggest to us that males do make more income based on similar education levels and AFQT scores.
```{r}
data$Male = ifelse(data$Gender=="male", 1, 0)
data$Female = ifelse(data$Gender=="female", 1, 0)
head(data)
case_lm1 = lm(log(Income2005)~AFQT+Educ+Male,data=data)
case_lm2= lm(log(Income2005)~AFQT+Educ+Female,data=data)
summary(case_lm1)
summary(case_lm2)
confint(case_lm1,level = .95)
```
Using the value given by the male Coefficient from the summary(case_lm1) command and the intercept given by the confint(case_lm,level=.95) we can find the percentage difference which is done below. Which shows us that the range of percentage of differences is between 6.99% and 7.32% is the average percent more income made by males in this survey.

```{r}
perc_dif_low = (0.6245)/(8.529)
perc_dif_high = (0.6245)/(8.932)
perc_dif_low
perc_dif_high
```



Below are two different set of graphs that show the correlation between the different columns in the dataset with income being log transformed. The second graph is a set of graphs with the same information but shared in a different way. The second set of graphs also give us a correlation value for the different combinations which tells us if there is a linear relationship with -1 being entirely negative and +1 being entirely positive. We can see the correlation between columns such as Education and AFGT have a high score of 0.595 which makes sense as more educated people probably would perform better on the exam.
```{r}
pairs((AFQT)~log((Income2005))+(Male)+(Female)+Educ, data=data,panel=panel.smooth,pch=20)
ggpairs(data,upper = list(continuous = wrap("cor",size = 5)))
#Putting in ggpairs just as a different visual representation.
#The correlation score has a lot to tell us about the relationships.
```



Below we can find the average value of income with both males and females based on their education levels. This isn't necessary to find the difference in income but it does give a data set of the income based on the degree of education which is easier to understand for most.
```{r}
data %>% 
  group_by(Gender, Educ) %>% 
  summarise(avg = mean(Income2005))
```




