---
title: 'ST 415 HMW # 5'
author: "Hank Greenburg"
date: "May 13, 2019"
output: word_document
---

# Problem 4.1

a)
Assumptions:
    12 Chambers, each chamber holds 12 seedlings, nearly uniform weight.
  
Experiment:
    To test the effects that CO2 concentration and temperature have on plant growth with the supplied resources I would have six of my chambers with the same CO2 levels but increased temperatures at constant intervals (i.e. 2 degrees rise in Celsius) with 12 seedlings in each. Then the other six chambers would have constant temperatures and increased CO2 at constant intervals. 
  
b)
To analyze the data I would use the Kruskal-Wallis test.
```{r}
#FINISH ME!!!
```


# Problem 4.2

a)
```{r}
chem = read.table("../data/chem.txt", header=TRUE);
str(chem);
head(chem);
## Boxplot
attach(chem)
boxplot(yield ~ alcohol, main="Percent Yield", xlab="Alcohol")
boxplot(yield ~ base, main="Percent Yield", xlab="Base");
```

b)
```{r}
m1 = lm(yield ~ base + alcohol)
anova(m1)
```
The ANOVA table above shows us the p-values are .1607 and .8881 for base and alcohol respectively. With such a high p-value,
there is a good chance that there isn't a corrilation between the yield and base/alcohol. This can also be seen in the data
set as well as the boxplot above. 



c)
```{r}
tapply(yield, base, "mean")
tapply(yield, alcohol, "mean")
```

d)
```{r}
SE = ((90.89167+89.85)-(91.0375+89.750))/(2)
SE
```
The effect on alcohol and base is a simple effect of base on alcohol. This is seen by how the slope difference between the two
is very nearly zero (.023). Which tells us there's a good chance that the two lines don't interact. 

e)
```{r}

```

f)
```{r}

```


# Problem 4.3

```{r}
## Load the full data set
plant = read.table("../data/plant.txt", header=TRUE);

## Extract data for species 2
ind = plant$species==2;
plant.new = plant[ind,];

## Inspect the data structure
str(plant.new);
plant.new;

## Attach the data frame
attach(plant.new);
```


a)
```{r}

```


b)

c)

d)

e)


# Problem 4.4

a)
```{r}

```


b)
```{r}

```

c)

