---
title: 'Homework #9'
author: "Hank Greenburg"
date: "June 3, 2020"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


## Fit a quadratic regression model in Year (no need to transform by subtracting 1900 and dividing by 100, as in the textbook), assuming independent data. Obtain the residuals from this model, and plot the residuals vs. Year. Does it appear that there is serial correlation in the residuals? Briefly explain.

Looking at the graph below it does appear that there is a serial correlation as we can see that there is a pattern of peaks and trophs throughout the years. These are pronounced enough that it is clear to see that there is a pattern and it's not random.


## Make a partial autocorrelation plot of the residuals. Does it appear the the residuals follow an AR(1) serial dependence model? Briefly explain.

Our `acf` graph does appear to be following the AR(1) serial dependence model. The trend of the plot follows a very similar trend to that of what we did in Lab #9.

## Calculate a point estimate of the first serial correlation coefficient $\alpha$ of the residuals.

These values are just below the acf plot.

## Perform the Cochrane-Orcutt procedure to estimate the regression coefficients in the presence of AR(1) autocorrelation. Include your code and summary() output.

Included below.

## Calculate 95% confidence intervals for the coefficients of Year and Year2. The confint() function doesn't work on orcutt objects, so you'll have to calculate these manually.

Final calculation made is this output.



```{r,warning=FALSE,message=FALSE,echo=FALSE,fig.width=5,fig.height=2.5}
library(Sleuth3)
library(ggplot2)
library(broom)
library(orcutt)
data = ex1507
Year2 <- (data$Year)^2
temp_lm <- lm(Temperature ~ Year+Year2,data=data)
resid <- augment(temp_lm)
qplot(Year, .resid, data=resid,
      xlab="Year", ylab="Residual Values")
```


```{r}
acf(resid$.resid) #ACF plot of our residuals
acf(resid$.resid, lag.max = 1,
    type="covariance", plot=FALSE) #Point estimate of our first serial correlation coeff

```



```{r}
co_lm <- cochrane.orcutt(temp_lm)
summary(co_lm)
```

```{r,warning=FALSE,message=FALSE,echo=FALSE}
#Manually calculating the confidence interval
z = 1.96 #z-value for our confidence interval
mean1 <- mean(data$Year) #mean for Year
mean2 <- mean(Year2) #Mean for year squared
sd1 <- sd(data$Year) # standard deviation of year
sd2 <- sd(Year2) # standard deviation of Years^2
n = 161 # number of years
print("Year 95% confidence interval:")
mean1+z*(sd1/sqrt(n))
mean1-z*(sd1/sqrt(n))
print("Year2 95% confidence interval:")
mean2+z*(sd2/sqrt(n))
mean2-z*(sd2/sqrt(n))
```






















