---
title: 'Homework #1'
author: "Hank Greenburg"
date: "March 31, 2020"
output:
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
Question 8.23:
Wine Consumption and Heart Disease. The data listed below are the average wine consumption rate(L/person) and number of ischemic heart disease deaths(per 1000 men aged 55-64) for 18 industrialized nations. Do the data suggest that the heart disease death rate is associated with wine consumption? If so how can that relationship be described? Do any countries have substantially higher or lower death rates?  

Here is the dataset:
```{r}
library(Sleuth3)
data = ex0823
data
```


Below is a graph representing the relationship between heart disease and wine consumption:
```{r}
Wine = log(data$Wine)
Heart = log(2*data$Mortality)
xlimit = c(0,5)
ylimit = c(1,4)
plot(Wine,Heart,xlab='Wine Consumption(L/Person)',xlim=xlimit,ylim=ylimit,
     ylab='Heart Disease(Men Aged 55-64)',pch=20)
abline(lm(Heart ~ Wine), col="red")
lines(lowess(Wine,Heart), col="blue")
```
From the above graph is does appear that there is a relationship between the amount of wine consumed and the heart disease rate but rather as a potentially positive relationship where the more wine consumed means the lower chance of heart disease in males between the ages of 55 and 64. The graph used is with logrithmic scales due to there being some skewing as shown in the graph below. There are a few data points that are drastically larger than the bulk of our data.

The graph without log scales:
```{r}
Wine = data$Wine
Heart = data$Mortality
xlimit = c(0,40)
ylimit = c(0,12)
plot(Wine,Heart,xlab='Wine Consumption(L/Person)',xlim=xlimit,ylim=ylimit,
     ylab='Heart Disease(Men Aged 55-64)',pch=20)
abline(lm(Heart ~ Wine), col="red")
lines(lowess(Wine,Heart), col="blue")
```

```{r}
data2 = lm((Wine)~(Heart))
summary(data2)
confint(data2, level = 0.95)
anova(data2)
```
Above is the summary, confidence interval(95%) and the ANOVA test for the data. We can see from the results that it is likely that there is a correlation between how much wine is consumed by an individual(male 55-64) and a decrease in their heart disease rate. 

